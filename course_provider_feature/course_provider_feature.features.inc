<?php
/**
 * @file
 * course_provider_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function course_provider_feature_node_info() {
  $items = array(
    'course_provider' => array(
      'name' => t('Course Provider'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
